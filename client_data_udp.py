import asyncio
import json
import socket
from aiortc import RTCPeerConnection, RTCConfiguration, VideoStreamTrack
from aiortc.contrib.signaling import CopyAndPasteSignaling, BYE


class VideoStream(VideoStreamTrack):     #esta clase es para que varias funciones posteriores no me den error, pero no es necessaria para lo que se pide en la practica
    def __init__(self):
        super().__init__()

    async def recv(self):
        pts, time_base = await self.next_timestamp()


        frame = await self.next_frame()
        return frame.to_ndarray(format="bgr24")

async def run_offer(pc, signaling):

    await signaling.connect()
    signaling_address = ('127.0.0.1', 8888)
    signaling_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    signaling_socket.sendto(b"REGISTER CLIENT", signaling_address)


    video_track = VideoStream()
    pc.addTrack(video_track)

    # Configurar la conexión de señalización
    async def signaling():
        while True:

            # Enviar oferta al servidor
            await pc.setLocalDescription(await pc.createOffer())
            sdp_offer = pc.localDescription.sdp
            signaling_socket.sendto(json.dumps({'sdp': sdp_offer, 'type': 'offer'}).encode(), signaling_address)

            # Recibir respuesta del servidor
            data, _ = await signaling_socket.recvfrom(4096)
            obj = json.loads(data.decode())
            await pc.setRemoteDescription(obj)

    await signaling()

if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    config = RTCConfiguration()
    pc = RTCPeerConnection(configuration=config)
    coro = run_offer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
