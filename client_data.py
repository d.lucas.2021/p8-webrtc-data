import asyncio
import time
import sdp_transform
import json
from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling


async def consume_signaling(pc, signaling):
    while True:
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)
        elif obj is BYE:
            print("Exiting")
            break


time_start = None


def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(pc, signaling):
    await signaling.connect()

    # aquí va el print del mensaje sdp que he extraído en un fichero aparte

    #sdp_dict = {
        #'sdp': 'v=0\r\no=- 3909497644 3909497644 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic:WMS *\r\nm=application 36556 DTLS/SCTP 5000\r\nc=IN IP4 212.128.255.11\r\na=mid:0\r\na=sctpmap:5000 webrtc-datachannel 65535\r\na=max-message-size:65536\r\na=candidate:7e2d2336d8acec921be7d2820bb70c68 1 udp 2130706431 212.128.255.11 36556 typ host\r\na=candidate:580b5f32035da7f14a30ea7a8d826c67 1 udp 2130706431 172.17.0.1 46420 typ host\r\na=candidate:0ad8db753210ce9554cf251a9e51e792 1 udp 1694498815 212.128.255.11 46420 typ srflx raddr 172.17.0.1 rport 46420\r\na=candidate:5c601615242769ed7ad5f253b64b26da 1 udp 1694498815 212.128.255.11 36556 typ srflx raddr 212.128.255.11 rport 36556\r\na=end-of-candidates\r\na=ice-ufrag:1P5U\r\na=ice-pwd:jFrtJdQ5fYKmGtTy2RbWva\r\na=fingerprint:sha-256 A7:8D:99:30:48:1B:90:61:F7:B2:01:AB:C3:F6:DD:21:CF:BF:88:AE:60:EC:FD:6A:76:8D:FE:60:EB:34:68:81\r\na=setup:actpass\r\n',
        #'type': 'offer'}

    #sdp_str = sdp_dict['sdp']
    #sdp_lines = sdp_str.split('\r\n')


    #sdp_formatted = ''
    #for line in sdp_lines:
        #sdp_formatted += line + '\n'

    #print(sdp_formatted)
    channel = pc.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")



    async def send_pings():
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())
    await signaling.send(pc.localDescription)

    await consume_signaling(pc, signaling)


if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    pc = RTCPeerConnection()
    coro = run_offer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
