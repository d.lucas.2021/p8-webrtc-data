import asyncio
import json
import socket

from aiortc import RTCPeerConnection
from aiortc.contrib.signaling import CopyAndPasteSignaling, BYE

async def run_answer(pc, signaling):
    await signaling.connect()

    signaling_address = ('127.0.0.1', 8888)
    signaling_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


    signaling_socket.sendto(b"REGISTER SERVER", signaling_address)
    print("-- Please enter a message from remote party --")


    while True:
        message_str, _ = await signaling_socket.recvfrom(4096)

        try:
            message = json.loads(message_str)
            if "sdp" in message and message["type"] == "offer":
                # Enviar la oferta al otro extremo
                await signaling.send(message)
                break
            elif message_str.strip().lower() == "bye":
                # Enviar mensaje BYE al otro extremo
                await signaling.send(BYE)
                break
            else:
                print("Invalid message. Please enter a valid SDP offer.")
        except json.JSONDecodeError:
            print("Invalid JSON. Please enter a valid JSON message.")


    async def signaling():
        while True:
            data, _ = await signaling_socket.recvfrom(4096)
            obj = json.loads(data.decode())

            await pc.setRemoteDescription(obj)
            if obj['type'] == 'offer':
                # Enviar respuesta
                await pc.setLocalDescription(await pc.createAnswer())
                sdp = pc.localDescription.sdp
                signaling_socket.sendto(json.dumps({'sdp': sdp, 'type': 'answer'}).encode(), signaling_address)

    await signaling()

if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    pc = RTCPeerConnection()
    coro = run_answer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())