import asyncio

from aiortc import RTCIceCandidate, RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE, CopyAndPasteSignaling


async def consume_signaling(pc, signaling):
    while True:
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            await pc.setRemoteDescription(obj)

            if obj.type == "offer":
                # send answer
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)
        elif obj is BYE:
            print("Exiting")
            break


time_start = None


async def run_answer(pc, signaling):
    await signaling.connect()

#aquí va el print del mensaje sdp que he extraído en un fichero aparte
    #sdp_dict = {"sdp": "v=0\r\no=- 3909498187 3909498187 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\na=group:BUNDLE 0\r\na=msid-semantic:WMS *\r\nm=application 43818 DTLS/SCTP 5000\r\nc=IN IP4 212.128.255.11\r\na=mid:0\r\na=sctpmap:5000 webrtc-datachannel 65535\r\na=max-message-size:65536\r\na=candidate:7e2d2336d8acec921be7d2820bb70c68 1 udp 2130706431 212.128.255.11 43818 typ host\r\na=candidate:580b5f32035da7f14a30ea7a8d826c67 1 udp 2130706431 172.17.0.1 34737 typ host\r\na=candidate:0ad8db753210ce9554cf251a9e51e792 1 udp 1694498815 212.128.255.11 34737 typ srflx raddr 172.17.0.1 rport 34737\r\na=candidate:5c601615242769ed7ad5f253b64b26da 1 udp 1694498815 212.128.255.11 43818 typ srflx raddr 212.128.255.11 rport 43818\r\na=end-of-candidates\r\na=ice-ufrag:jOe0\r\na=ice-pwd:DNGzMaaI9ErfwtOleS1eYv\r\na=fingerprint:sha-256 D2:83:84:64:EA:9F:6E:8D:F8:0E:CF:53:E7:2F:E8:98:32:5B:06:F6:49:F5:34:49:AE:42:3A:CD:6A:45:12:BE\r\na=setup:active\r\n", "type": "answer"}



    #sdp_str = sdp_dict['sdp']
    #sdp_lines = sdp_str.split('\r\n')

    #sdp_formatted = ''
    #for line in sdp_lines:
        #sdp_formatted += line + '\n'

    #print(sdp_formatted)

    @pc.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")
            print(message)
            if isinstance(message, str) and message.startswith("ping"):
                # reply
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    await consume_signaling(pc, signaling)


if __name__ == "__main__":
    signaling = CopyAndPasteSignaling()
    pc = RTCPeerConnection()
    coro = run_answer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
