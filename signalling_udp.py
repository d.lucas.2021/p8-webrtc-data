import asyncio
import json
import socket

async def signaling_udp():

    registered_endpoints = {"client": None, "server": None}


    server_address = ('127.0.0.1', 8888)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(server_address)

    while True:
        data, addr = sock.recvfrom(4096)
        message = data.decode()


        if message.startswith('REGISTER CLIENT') or message.startswith('REGISTER SERVER'):
            print(f"Registered: {message} from {addr}")
            # Almacenar la dirección IP y el puerto
            endpoint_type = message.split()[1].lower()
            registered_endpoints[endpoint_type] = addr
        else:

            try:
                sdp_obj = json.loads(message)
                print(f"Received SDP message from {addr}: {sdp_obj}")


                other_endpoint_type = "server" if addr == registered_endpoints["client"] else "client"
                other_endpoint_addr = registered_endpoints[other_endpoint_type]

                if other_endpoint_addr:

                    sock.sendto(data, other_endpoint_addr)
                    print(f"Forwarded SDP message to {other_endpoint_type}: {other_endpoint_addr}")
                else:
                    print(f"No registered {other_endpoint_type} to forward SDP message.")
            except json.JSONDecodeError as e:
                print(f"Error decoding JSON: {e}")

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(signaling_udp())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()
